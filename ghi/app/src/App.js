import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './Manufacturer/ManufacturerList';
import VehicleList from './Inventory/VehicleList';
import ManufacturerForm from './Manufacturer/ManufacturerForm';
import VehicleForm from './Inventory/VehicleForm';
import InventoryList from './Inventory/InventoryList';
import Technician from './Appointments/Technician';
import ServiceAppointmentList from './Appointments/AppointmentList';
import AutomobileForm from './Inventory/InventoryForm';
import SalesCustomerForm from './Sales/SalesCustomerForm';
import SalespersonForm from './Sales/SalespersonForm';
import SalesRecordList from './Sales/SalesRecordsList';
import SalesRecordForm from './Sales/SalesRecordForm';
import ErrorPage from './ErrorPage';
import AppointmentForm from './Appointments/AppointmentForm';
import History from './Appointments/ServiceHistory';
function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div >
        <Routes>
          <Route path="*" element={<ErrorPage/>} />
          <Route path="/" element={<MainPage />} />
          <Route path='vehicles'>
            <Route path='' element={<VehicleList/>} />
            <Route path='new' element={<VehicleForm />} />
          </Route>
          <Route path="inventory">
            <Route path='' element={<InventoryList />}  />
            <Route path='new' element={<AutomobileForm automobiles={props.vehicles} />} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<ServiceAppointmentList/>} />
            <Route path='new' element={<AppointmentForm/>} />
          </Route>
          <Route path='manufacturers'>
            <Route path="" element={<ManufacturerList />}  />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path='technician' element={<Technician/>}/>
          <Route path="customer/new" element={<SalesCustomerForm/>}/>
          <Route path="salesperson/new" element={<SalespersonForm/>}/>
          <Route path="salesrecords">
            <Route path="" element={<SalesRecordList/>} />
            <Route path="new" element={<SalesRecordForm/>}/>
          </Route>
          <Route path="records" element={<History/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
