import React, { useEffect, useState } from 'react';
import "../styles/index.css"
function AutomobileForm() {
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVin] = useState('');
  const [model, setModel] = useState('');
  const [models, setModels] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {}
    data.color = color
    data.year = year
    data.vin = vin
    data.model_id = model
    const automobileUrl = 'http://localhost:8100/api/automobiles/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
    }
    const automobileResponse = await fetch(automobileUrl, fetchConfig)
    if (automobileResponse.ok) {

      setColor('');
      setYear('');
      setVin('');
      setModel('');
    }
  }
  const handleColorChange = (event) => {
    const value = event.target.value
    setColor(value)
  }

  const handleYearChange = (event) => {
    const value = event.target.value
    setYear(value)
  }

  const handleVinChange = (event) => {
    const value = event.target.value
    setVin(value)
  }

  const handleModelChange = (event) => {
    const value = event.target.value
    setModel(value)
  }

  const fetchModelData = async () => {
    const modelUrl = 'http://localhost:8100/api/models/'
    const modelResponse = await fetch(modelUrl);
    if (modelResponse.ok) {
      const modelData = await modelResponse.json();
      setModels(modelData.models)
    }
  }
  useEffect(() => {
    fetchModelData()
  }, [])
  return (
    <div className="mx-auto w-1/4 p-5" id='test' >
      <div className="shadow p-4 mx-auto mt-4">
        <p className='text-xl font-semibold text-center p-2'>Add an Automobile to the Inventory</p>
        <form onSubmit={handleSubmit} >
          <div className='form-floating mb-3'>
            <input value={color} onChange={handleColorChange} name="color" placeholder="Color" id="color" required type="text" className="form-control" />
            <label>Color</label>
          </div>
          <div className='form-floating mb-3'>
            <input value={year} onChange={handleYearChange} maxLength="4" name="year" placeholder="Year" id="year" required type="text" className="form-control" />
            <label>Year</label>
          </div>
          <div className='form-floating mb-3'>
            <input value={vin} onChange={handleVinChange} maxLength="17" name="vin" placeholder="VIN Number" id="name" required type="text" className="form-control" />
            <label>VIN Number</label>
          </div>
          <div className="form-floating mb-3">
            <select onChange={handleModelChange} value={model} id="model_id" name="auto_id" className="form-select">
              <option value=''>Choose a model</option>
              {models.map(model => {
                return (
                  <option key={model.id} value={model.id}>
                    {model.name}
                  </option>
                )
              })}
            </select>
          </div>
          <div className='flex justify-center'>
          <button className="bg-[#985F99] rounded-full text-center p-3 font-semibold text-[#F6F9EE]">Add Automobile</button>
          </div>
        </form>
      </div>

    </div>

  )
}//end of AutomobileForm function
export default AutomobileForm
