import React, { useEffect, useState } from 'react';

function VehicleCard(props){
    return(
        <div className='col p-5 w-full h-auto'>
            {props.list.map(vehicle => {
                return (
                    <div key={vehicle.id} className="card text-center mt-5 hover:-translate-y-5 shadow-md" >
                        <figure className=''><img src={vehicle.picture_url}  className="object-scale-down" alt="Card image cap" /></figure>
                        <div className='card-body p-2 text-xl'>
                            <h5 className="">{vehicle.name}</h5>
                            <p className=""><small className="text-muted">{vehicle.manufacturer.name}</small></p>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

function VehicleList(){
     const[vehicles, setVehicle] = useState([],)
     const [vehicleCard, setVehicleCards] = useState([],[],[],[])

     const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'
        try{
            const response = await fetch(url)
            if(response.ok){
            const data = await response.json()
            setVehicle(data.models)

            const requests = [];
            for (let model of data.models) {
              const detailUrl = `http://localhost:8100${model.href}`;
              requests.push(fetch(detailUrl));
            }
            // Wait for all of the requests to finish
            // simultaneously
            const responses = await Promise.all(requests);
            // Set up the "columns" to put the conference
            // information into
            const columns = [[], [], [], []];
            // Loop over the conference detail responses and add
            // each to to the proper "column" if the response is
            // ok
            let i = 0;
            for (const modelResponse of responses) {
              if (modelResponse.ok) {
                const details = await modelResponse.json();
                columns[i].push(details);
                i = i + 1;
                if (i > 3) {
                  i = 0;
                }
              } else {
                console.error(modelResponse);
              }
            }

            // Set the state to the new list of three lists of
            // conferences
            setVehicleCards(columns);

            }
        } catch (e) {
            console.error(e)
        }
    }

    useEffect( () =>{
        fetchData()
    }, [])


    return (
        <div className='min-h-screen bg-[#F6F9EE]'>
        <div className="flex row">
        <h1 className='text-center text-4xl font-semibold pt-5'>All Available Car Models</h1>
            {vehicleCard.map((vehicleList, index) =>{
                return (
                    <VehicleCard key={index} list={vehicleList} />
                )
            })}
        </div>
        </div>

    )
}
export default VehicleList;
