import React from "react";
export default function MainPage() {



  return (
    <div className="hero min-h-screen brightness-90" style={{ backgroundImage: `url("https://images.pexels.com/photos/754595/pexels-photo-754595.jpeg")` }}>
  <div className="hero-overlay  bg-opacity-20 "></div>
  <div className="hero-content  text-center text-white">
    <div className="max-w-md">
      <h1 className="mb-5 text-7xl font-bold ">Welcome to Car Car</h1>
      <p className="mb-5 text-3xl ">Let us help you find your dream car</p>
    </div>
  </div>
</div>
  )
};
