import React, { useState } from 'react';

function ManufacturerForm() {

  const [manufacturer, setManufacturer] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = manufacturer

    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      const newManufacturer = await response.json();
      setManufacturer('');
    }
  }
  const handleManufacturerChange = event => {
    const value = event.target.value
    setManufacturer(value)
  }

  return (
    <>

      <div className='min-h-screen  '>
        <div className='py-48'>
          <div className="mx-auto rounded-xl p-2 manufacturer box-shadow max-w-6xl object-fill" style={{backgroundImage : 'url("https://media.designrush.com/articles/1651/conversions/_1526480503_147_car-preview.jpg")', objectFit: 'cover'  }}>
            <form onSubmit={handleSubmit} id="manufacturer-form" className="max-w-sm mt-16 rounded-xl shadow-xl mx-auto bg-white ">
                <div className='flex rounded-t-lg bg-red-500 font-bold text-xl justify-center p-2 '><p>Add a Manufacturer</p></div>
                <div className="flex flex-col mt-12">
                <label className='block font-bold ml-10' htmlFor='manufacturer-input'>New Manufacturer</label>
                <input value={manufacturer} onChange={handleManufacturerChange} id="manufacturer-input" placeholder="Write the new Manufacturer's Name..." required type="text" className="form-control" name="name" />
                </div>
              <button className="rounded-full p-2 p flex mx-auto bg-[#F94A29]/90 hover:bg-[#F94A29] px-4 font-bold mt-12">Create</button>
            </form>
          </div>
          </div>
        </div>

    </>
  )
};
export default ManufacturerForm;
