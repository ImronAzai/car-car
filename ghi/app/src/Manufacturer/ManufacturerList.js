import React, { useEffect, useState } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturer] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        try {
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setManufacturer(data.manufacturers)
            }
        } catch (e) {
            console.error(e)
        }
    }
    useEffect(() => {
        fetchData()
    }, [])
    return (
        <>
            <div className="min-h-screen bg-white flex justify-center ">
                <table className="table max-w-sm table-zebra min-w-[50rem] mt-24 ">
                    <thead>
                        <tr className='text-center  bg-[#408E91]'>
                            <th className='text-lg text-white'>Vehicle Manufacturers</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {manufacturers.map((manufacturer) => {
                                return (
                                    <tr className='border-2 border-black' key={manufacturer.id}>
                                        <td className='bg-white text-2xl w-[50rem] text-center font-medium rounded-none mx-auto px-48 justify-center'>{manufacturer.name}</td>
                                    </tr>
                                )
                            })}
                        </tr>
                    </tbody>
                </table>
            </div>

        </>
    )
}
export default ManufacturerList;
