import { NavLink, Link } from 'react-router-dom';
import "./styles/index.css"


function Nav() {
  return (
    <>
   <style>
    @import url('https://fonts.googleapis.com/css2?family=Kaisei+Opti&display=swap');
   </style>

<div className="navbar border-b-2 border-black   kaisei z-10 font-semibold bg-[#F9F6EE] text-stone-900">
  <div className="navbar-start">
    <NavLink to="/" className="btn btn-ghost normal-case text-xl">Car Car</NavLink>
    <ul className="menu menu-horizontal px-1 ">
      <li tabIndex={0}>
        <p className="hover:bg-black hover:text-white" >
        Manufacturer
      </p>
        <ul className="p-2 bg-[#F9F6EE]">
          <li><Link to="manufacturers"  className="hover:bg-black hover:text-white">View Manufacturers</Link></li>
          <li><NavLink to="/manufacturers/new"  className="hover:bg-black hover:text-white">Create a Manufacturer</NavLink></li>
        </ul>
      </li>
      <li tabIndex={0}>
        <p className="hover:bg-black hover:text-white" >
        Vehicles
      </p>
        <ul className="p-2 bg-[#F9F6EE]">
          <li><Link to="vehicles"  className="hover:bg-black hover:text-white">In Stock Models</Link></li>
          <li><NavLink to="/vehicles/new"  className="hover:bg-black hover:text-white">Add a Vehicle Model</NavLink></li>
        </ul>
      </li>
      <li tabIndex={0}>
        <p className="hover:bg-black hover:text-white" >
        Inventory
      </p>
        <ul className="p-2 bg-[#F9F6EE]">
          <li><Link to="inventory"  className="hover:bg-black hover:text-white">Inventory</Link></li>
          <li><NavLink to="/inventory/new"  className="hover:bg-black hover:text-white">Update Inventory</NavLink></li>
        </ul>
      </li>
      <li tabIndex={0}>
        <p className="hover:bg-black hover:text-white" >
        Appointments
      </p>
        <ul className="p-2 bg-[#F9F6EE]">
          <li><Link to="appointments"  className="hover:bg-black hover:text-white">View Appointments</Link></li>
          <li><NavLink to="/appointments/new"  className="hover:bg-black hover:text-white">Schedule Service Appointment</NavLink></li>
        </ul>
      </li>
      <li><NavLink to='records' className="hover:bg-black hover:text-white">Service History</NavLink></li>
      <li tabIndex={0}>
        <p className="hover:bg-black hover:text-white" >
        Sales
      </p>
        <ul className="p-2 bg-[#F9F6EE]">
          <li><Link to="salesrecords"  className="hover:bg-black hover:text-white">Sales Records</Link></li>
          <li><NavLink to="/salesrecords/new"  className="hover:bg-black hover:text-white">Record a Sale</NavLink></li>
          <li><NavLink className='nav-link' to="customer/new">Create Customer</NavLink></li>
        </ul>
      </li>
      <li tabIndex={0}>
        <p className="hover:bg-black hover:text-white" >
        Personnel
      </p>
        <ul className="p-2 bg-[#F9F6EE]">
          <li><NavLink className='nav-link' to="technician">Register a Technician</NavLink></li>
          <li><NavLink className='nav-link' to="salesperson/new">Create Salesperson</NavLink></li>
        </ul>
      </li>
    </ul>
  </div>
</div>




    </>
  )
}

export default Nav;
